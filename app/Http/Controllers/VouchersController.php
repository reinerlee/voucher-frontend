<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;

class VouchersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

	
    public function __construct()
    {
    }

    //
	
	public function generate(Request $request) {
	    $validatedData = $request->validate([
			'number' => 'bail|required|integer|max:1000000',
		]);
		
		$postData = $request->all();

		$response = Http::post('http://backend.reiner.local/generateCodes', [
			'number' => $postData['number'],
		]);
		
		$result = $response->body();
		$result = json_decode($result , true);
		
		return view('results', ['execution_time' => $result['execution_time'] , 'url' => './codes/'. $result['session_id'] ]);
	
	}
	
	
	public function getList(Request $request, $session) {
		
		$postData = $request->all();

		$response = Http::get('http://backend.reiner.local/codes/'.$session, [
		]);
		$results = json_decode($response->body(),true);	
		
		//return view('codes', ['results' => $response->body()]);
		return view('codes', ['vouchers' => $results]);
				
	}	
	
}
